import motor.motor_asyncio
from bson.objectid import ObjectId
from fastapi import HTTPException
from starlette import status
from starlette.responses import JSONResponse

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.test1

todo_collection = database.get_collection("todo")


# helpers


def todo_helper(todo) -> dict:
    return {
        "id": str(todo["_id"]),
        "title": todo["title"],
        "description": todo["description"],
        "created_date":todo["created_date"],
        "updated_date":todo["updated_date"]

    }


# crud operations

# Retrieve all students present in the database
async def retrieve_todos():
    todos = []
    async for todo in todo_collection.find():
        todos.append(todo_helper(todo))
    return todos


# Add a new student into to the database
async def add_todo(todo_data: dict) -> dict:
    todo = await todo_collection.insert_one(todo_data)
    new_todo = await todo_collection.find_one({"_id": todo.inserted_id})
    return todo_helper(new_todo)


# Retrieve a student with a matching ID
async def retrieve_todo(id: str) -> dict:
    todo = await todo_collection.find_one({"_id": ObjectId(id)})
    if todo:
        return todo_helper(todo)


# Update a student with a matching ID
async def update_todo(id: str, data: dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    todo = await todo_collection.find_one({"_id": ObjectId(id)})
    if todo:
        updated_todo = await todo_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_todo:
            return True
        return False


async def delete_todo(id: str):
    # Return false if an empty request body is sent.
    delete_result = await todo_collection.delete_one({"_id": ObjectId(id)})
    return  delete_result
