from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from app.server.database import (
    add_todo,
    delete_todo,
    retrieve_todo,
    retrieve_todos,
    update_todo,
)
from app.server.models.todos import (
    ErrorResponseModel,
    ResponseModel,
    ToDoSchema,
    UpdateToDoModel,
)

router = APIRouter()


@router.post("/", response_description="todo data added into the database")
async def add_todo_data(todo: ToDoSchema = Body(...)):
    todo = jsonable_encoder(todo)
    new_todo = await add_todo(todo)
    return ResponseModel(new_todo, "todo added successfully.")


@router.get("/", response_description="Todos retrieved")
async def get_todos():
    todos = await retrieve_todos()
    if todos:
        return ResponseModel(todos, "todos data retrieved successfully")
    return ResponseModel(todos, "Empty list returned")


@router.get("/{id}", response_description="Student data retrieved")
async def get_todo_data(id):
    todo = await retrieve_todo(id)
    if todo:
        return ResponseModel(todo, "Todo data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Student doesn't exist.")


@router.put("/{id}")
async def update_todo_data(id: str, req: UpdateToDoModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_todo = await update_todo(id, req)
    if updated_todo:
        return ResponseModel(
            "Todo with ID: {} name update is successful".format(id),
            "Todo name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the todo data.",
    )


@router.delete("/{id}", response_description="Todo data deleted from the database")
async def delete_todo_data(id: str):
    deleted_todo = await delete_todo(id)
    if deleted_todo and deleted_todo.deleted_count == 1:
        return ResponseModel(
            "todo with ID: {} removed".format(id), "todo deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Todo with id {0} doesn't exist".format(id)
    )
