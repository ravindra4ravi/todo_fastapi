from fastapi.testclient import TestClient

from app.server.app import app

client = TestClient(app)


def test_read_todos():
    response = client.get("/todo/")
    assert response.status_code == 200


#

def test_add_todo():
    response = client.post(
        "/todo/",
        json={"title": "event1", "description": "event1 description"},
    )
    assert response.status_code == 200
    result = response.json().get("data", [{}])[0]
    result.pop('created_date')
    result.pop('updated_date')
    result.pop('id')
    assert result == {"title": "event1", "description": "event1 description"}

