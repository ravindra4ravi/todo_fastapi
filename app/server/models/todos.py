from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field


class ToDoSchema(BaseModel):
    title: str = Field(...)
    description: str = Field(...)
    created_date: Optional[int] = datetime.now().timestamp()
    updated_date: Optional[int] = datetime.now().timestamp()

    class Config:
        schema_extra = {
            "example": {
                "title": "Evening event",
                "description": "Lorem Ipsum",
                "created_date": 65656565,
                "updated_date": 65656565,
            }
        }


class UpdateToDoModel(BaseModel):
    title: Optional[str]
    description: Optional[str]
    created_date: Optional[int]
    updated_date: Optional[int]

    class Config:
        schema_extra = {
            "example": {
                "title": "Evening event",
                "description": "Lorem Ipsum",
                "created_date": 65656565,
                "updated_date": 65656565,
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
