Requirements:
#Python3.8
# MongoDB

Install virtualEnv:
 **sudo pip3 install virtualenv** 

Create virtualenv Environment:

**virtualenv --python=/usr/bin/python3.8 env**


Run requirement.txt to install the app dependencies:

pip3 install requirements.txt 

Run:

python app/main.py

Swagger API URL :

http://0.0.0.0:8000/docs

Run using Docker:
sudo docker-compose build 
sudo docker-compose up



